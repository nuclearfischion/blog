# web-nodejs-sample

ExpressJS Sample Application

# Developer Workspace
[![Contribute](http://beta.codenvy.com/factory/resources/codenvy-contribute.svg)](http://beta.codenvy.com/f?id=r8et9w6vohmqvro8)

# Stack to use

FROM [codenvy/node](https://hub.docker.com/r/codenvy/node/)

# How to run

| #       | Description           | Command  |
| :------------- |:-------------| :-----|
| 1      | Run | `cd ${current.project.path}/app && node app.js` |

# GitHub Flavored Markdown
https://github.github.com/gfm/#introduction

# RESTful Routing
## Introduction
* Definte REST and explain WHY it matters
* List all 7 RESTful routes
    ** https://codepen.io/cheryl-fischer/full/vVrjaM/
* Show example of RESTful routing in practice

## Blog Index
* Setup the Blog App
* Create the Blog model
* Add INDEX route and template

## Basic Layout
* Add HEader and Footer Partials
* Include Semantic UI
* Add Simple Nav

## Putting the C in Crud
* Add NEW routes
* Add NEW template
* Add CREATE route
* Add CREATE template

## SHOWtime
* Add Show route
* Add Show template
* Add links to show page
* Style show template

## Edit/Update
* Add Edit Route
* Add Edit Form
* Add Update Route
* Add Update Form
* Add Method-Override   *Replace default GET/POST methods in HTML forms with PUT or DELETE requests*

## DESTROYYYYYY
* Add Destroy Route
* Add Edit and Destroy Links

## Final Updates
* Sanitize blog body
* Style Index
* Update REST Table

## Cheryl's Personal Touches
* Added rich text editor using Summernote (https://summernote.org/getting-started/)
    - note: npm install didn't work but copying and pasting from above link worked.
