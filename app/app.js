/*******************************Include Modules Needed For App**********************************/
var express             = require('express');
var cherylsModule       = require("cheryls-test-module/cheryls-test-module.js");
var bodyParser          = require("body-parser");
var expressSanitizer    = require("express-sanitizer");
var mongoose            = require("mongoose");
var methodOverride      = require("method-override");

/***************************************Express Stuff******************************************/
var app = express();                    //create an Express instance
app.use(express.static('public'));      //serve static files from public directory

/***************************************Database Stuff******************************************/
//define a connection
mongoose.connect("mongodb://localhost/restful_blog_app", { useNewUrlParser: true });

//define blog schema
var blogSchema = new mongoose.Schema({
    title: String,
    image: String,
    body: String,
    created: {type: Date, default: Date.now}
});

//compile model
var Blog = mongoose.model("Blog", blogSchema);

/*************************************Setting Middleware****************************************/
app.set("view engine", "ejs");          //set the view engine to ejs
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSanitizer());
app.use(methodOverride("_method"));

/************************************Express RESTful Routes***************************************/
app.get('/', function (req, res) {
    console.log(cherylsModule.catchphrase);
    res.redirect("/blogs");
});

// INDEX ROUTE /blogs
app.get("/blogs", function(req, res){
    //find and return all blog posts; passing in empty query to return everything
    Blog.find({}, function(err, blogs){
        if(err)
            console.log("error");
        else
            res.render("index", {blogs: blogs});    
    });
}); 

// NEW ROUTE /blogs/new         GET
app.get("/blogs/new", function(req, res){
    res.render("new");
});

// CREATE ROUTE /blogs          POST
app.post("/blogs", function(req, res){
    console.log("-----someone created a new blog post-----");
    //create new blog object based on req
    console.log("blog title: " + req.body.blog.title);
    
    //sanitize that blog body!
    console.log("blog body before sanitizing: " + req.body.blog.body);
    req.body.blog.body = req.sanitize(req.body.blog.body);
    console.log("blog body after sanitizing: " + req.body.blog.body);
    
    Blog.create(req.body.blog, function(err, newPost){
        if(err)
            res.redirect("/blogs/new");
        else
            res.redirect("/blogs");
    });
    
});

// SHOW ROUTE /blogs/:id        GET
app.get("/blogs/:id", function(req, res){
    console.log("someone's on the show page");
    let id = req.params.id;
    Blog.findById(id, function(err, post){
        if(err){
            console.log("had trouble finding post");
            console.log(err);
        }

        else{
            console.log("SHOW ROUTE returned post with title: " + post.title);
            res.render("show", {post:post});
        }
    });
});
// EDIT ROUTE /blogs/:id/edit   GET
app.get("/blogs/:id/edit", function(req, res){
    let id = req.params.id;
    Blog.findById({"_id": id}, function(err, post){
        if(err)
            console.log("something's gone wrong on EDIT ROUTE");
        else
            res.render("edit", {post:post});
    });
});
// UPDATE ROUTE /blogs/:id      PUT
app.put("/blogs/:id", function(req, res){
    let id = req.params.id;
    
    //sanitize that blog body!
    console.log("blog body before sanitizing: " + req.body.blog.body);
    req.body.blog.body = req.sanitize(req.body.blog.body);
    console.log("blog body after sanitizing: " + req.body.blog.body);
    
    let blog = req.body.blog;
    console.log("updating " + blog.title);
    Blog.updateOne({"_id": id}, {$set: blog}, function(err, post){
        if(err)
            console.log("someething has gone wrong on the UPDATE route");
        else
            res.redirect("/blogs/" + id);    
    });
});
// DESTROY ROUTE /blogs/:id     DELETE
app.delete("/blogs/:id", function(req, res){
    console.log("delete route reached!");
    let id = req.params.id;
    console.log("req.params.id = " + id);
    
    Blog.findById({"_id": id}, function(err, post){
        console.log("This post is scheduled for deletion: " + post.title);
        Blog.deleteOne({"_id": id}, function(err, post){
            if(err)
                console.log("couldn't delete");
            else
                console.log("deleted! :D");
        });
    });
    
    res.redirect("/blogs");
});

//listen on port 3000 for requests
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
